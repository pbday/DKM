
select r.party, avg(r.ukvotes) as average
from ukresults r
group by r.party
having count(*) > 10
order by average asc
;

