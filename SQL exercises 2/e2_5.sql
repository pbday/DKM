
select distinct s.sname, s.city
from supplier s
where not exists (select * 
					from part p, supply x
					where p.colour <> 'RED' and s.s# = x.s# and p.p# = x.p#);