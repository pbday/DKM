--Find constituency names and percentage of voters voting in constituencies
--where at least 85% of registered voters actually voted. Order result rows by
--decreasing turnout.

select c.ukarea , r.votes * 100 / c.ukelectors as turnout
from ukconsts c , (select r1.uknum, sum(r1.ukvotes) as votes from ukresults r1 group by r1.uknum) r
where c.uknum = r.uknum
and r.votes / c.ukelectors > 0.85
order by turnout desc
;

