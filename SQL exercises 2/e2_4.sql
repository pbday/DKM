
select s.sname, s.city
from supplier s
where not exists 
	(select * 
	from part p
	where p.colour = 'RED' and
		not exists (select * from supply x where x.s# = s.s# and x.p# = p.p#));