--(i) Find the name of each supplier who supplies a green part.

--
	
--Algebraic formula:
--(( SUPPLIER JOIN PART ) WHERE COLOUR = 'GREEN' ) [SNAME]

select distinct sname
	from supplier S, part P, supply L
	where S.S# = L.S# and P.P# = L.P# and P.colour = 'GREEN'
	;



--(ii) Find the city of each project supplied by a Paris-based supplier.
--((( PROJECT JOIN SUPPLY ) [CITY] )
--JOIN ( SUPPLIER WHERE CITY = 'PARIS' )) [CITY]

select distinct j.city
	from supplier s, supply link, project j 
	where s.s# = link.s# and j.j# = link.j#
		and s.city = 'PARIS'
	;

--(iii) Find the name of each supplier who does not supply a Paris-based project.
--(((( SUPPLY JOIN PROJECT ) WHERE CITY <> 'PARIS') [S#] )
--JOIN SUPPLIER ) [SNAME]

select distinct s.sname
	from supplier s 
		where s.s# not in (select x.s#
								from supply x, project y
								where x.j# = y.j# and y.city = 'PARIS'
						)
	; 

--(iv) Find the number of each part supplied by both a Paris-based supplier and an
--Athens-based supplier.

select distinct p.p#
	from part p , supply link1, supply link2  
	where p.p# = link1.p# and p.p# = link2.p#
		and link1.s# in (select s# from supplier where city = 'PARIS')
		and link2.s# in (select s# from supplier where city = 'ATHENS')
		;

--(( SUPPLIER JOIN SUPPLY )
--WHERE CITY = 'PARIS' AND CITY = 'ATHENS' ) [P#]

--(v) Find the number of each part with weight greater than 15 supplied to all the projects.
--((( PART WHERE WEIGHT > 15 ) JOIN SUPPLY ) JOIN PROJECT ) [P#]

select p.p#
	from part p, supply link
	where p.weight > 15 and p.p# = link.p#
	group by p.p#
	having count(distinct link.j#) = (select count(distinct p.j#)
													from project p
													)
	;