--Exercise 4

--Consider the Suppliers-Parts-Projects database with additional table CITIES created in
--SQL Exercise 3.
--SUPPLIER (S#, SNAME, STATUS, CITY)
--PART (P#, PNAME, COLOUR, WEIGHT,
--CITY) PROJECT (J#, JNAME, CITY)
--SUPPLY (S#, P#, J#, QUANTITY)
--CITIES (CITY, SUPPLIER_COUNT, PART_COUNT, PROJECT_COUNT, STATUS)
--Recall that for each city, CITIES records the number of suppliers, number of parts and
--number of projects based in that city (any of which may be 0) as well as a status value for
--the city. Some status values are not known and are recorded as null values. You may
--assume that CITIES records all possible CITY values.
--SQL> SELECT * FROM CITIES;
--CITY SUPPLIER_COUNT PART_COUNT PROJECT_COUNT STATUS
--------------- -------------- ---------- ------------- ---------
--ATHENS 1 0 2 30
--LONDON 2 3 2
--OSLO 0 0 1
--PARIS 2 2 1 30
--ROME 0 1 1 40
--TALLINN 0 0 0
--VIENNA 0 0 0
--7 rows selected.

--Write SQL statements to carry out the following changes to the tables, remembering that
---the tables store all strings in upper case letters e.g. 'LONDON' not 'London':

--(i) Create a new row in CITIES for a city Wigan. There are no suppliers, parts or
--projects based in Wigan, and the status of the city is 15.

insert into cities 
values ('WIGAN' , 0,0,0,15);

--(ii) Create a new row in CITIES for a city Zagreb. There are no suppliers, parts or
--projects based in Zagreb, and the status of the city is not known.

insert into cities (city, supplier_count, part_count, project_count)
values ('ZAGREB' , 0,0,0);

--(iii) Create a new row in SUPPLIER for a supplier with S# value S6. The supplier’s
--name is Gill, their status is 10 and their city is Zagreb.

insert into supplier 
values ('S6','GILL',10,'ZAGREB');


--(iv) Amend PART to remove any rows for parts based in Paris.

delete from part
where city = 'PARIS';

--(v) Amend PROJECT to reflect that project J6 is now based in Vienna.

update project 
set city = 'VIENNA'
where j# = 'J6'
;

--(vi) Amend CITIES to remove any rows for cities which are of unknown status and have
--no suppliers, parts or projects based there.


delete from cities c
where (select count(*) from supplier s where c.city = s.city) = 0
	and (select count(*) from part p where c.city = p.city) = 0
	and (select count(*) from project j where c.city = j.city) = 0
	and status is null
	;

--(vii) Amend CITIES so that the status column reflects a new policy regarding unknown
--status values: if the city has a supplier based there, the unknown status should be set
--to the minimum status value of any supplier based in that city, otherwise the
--status should remain as unknown. Existing known status values should not be
--amended.

update cities c
set c.status = (select min(s.status) from supplier s where s.city = c.city)
where c.status is null
	and (select count(*) from supplier s where s.city = c.city) <> 0;

--(viii) Amend CITIES so that the count columns reflect the changes to CITY values in
--SUPPLIER, PART and PROJECT following execution of the statements above in
--that order.

update cities c
set c.supplier_count = (select count(*) from supplier s where s.city = c.city),
	c.part_count = (select count(*) from part p where p.city = c.city),
	c.project_count = (select count(*) from project j where j.city = c.city)
;

CITY            SUPPLIER_COUNT PART_COUNT PROJECT_COUNT     STATUS
--------------- -------------- ---------- ------------- ----------
ATHENS                       1          0             2         30
LONDON                       2          3             2         20
PARIS                        2          0             1         30
ROME                         0          1             1         40
VIENNA                       0          0             1
WIGAN                        0          0             0         15
ZAGREB                       1          0             0         10

7 rows selected.


--After executing your statements, CITIES should contain the following rows:
--SQL> SELECT * FROM CITIES;
--CITY SUPPLIER_COUNT PART_COUNT PROJECT_COUNT STATUS
--------------- -------------- ---------- ------------- ----------
--ATHENS 1 0 2 30
--LONDON 2 3 2 20
--PARIS 2 0 1 30
--ROME 0 1 1 40
--VIENNA 0 0 1
--WIGAN 0 0 0 15
--ZAGREB 1 0 0 10
--7 rows selected.