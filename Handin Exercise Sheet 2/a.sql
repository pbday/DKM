/*
describe part; 
----------------------------------------- -------- ----------------------------
 P#                                        NOT NULL VARCHAR2(6)
PNAME                                     NOT NULL VARCHAR2(20)
 COLOUR                                    NOT NULL VARCHAR2(6)
 WEIGHT                                    NOT NULL NUMBER
 CITY                                      NOT NULL VARCHAR2(15)

describe supplier; 
----------------------------------------- -------- ----------------------------
 S#                                        NOT NULL VARCHAR2(5)
 SNAME                                     NOT NULL VARCHAR2(20)
 STATUS                                    NOT NULL NUMBER
 CITY                                      NOT NULL VARCHAR2(15)

describe project; 
----------------------------------------- -------- ----------------------------
 J#                                        NOT NULL VARCHAR2(4)
 JNAME                                     NOT NULL VARCHAR2(10)
 CITY                                      NOT NULL VARCHAR2(15)
 
describe supply;
----------------------------------------- -------- ----------------------------
 S#                                        NOT NULL VARCHAR2(5)
 P#                                        NOT NULL VARCHAR2(6)
 J#                                        NOT NULL VARCHAR2(4)
 QUANTITY                                           NUMBER
*/


--(a) Find the name and weight of each red part.

select p.pname , p.weight
from part p
where p.colour='RED'
;
/*
PNAME                    WEIGHT
-------------------- ----------
NUT                          12
SCREW                        14
COG                          19
*/


--(b) Find the name and status of each supplier who supplies project J2.

select distinct s.sname, s.status
from supplier s, supply x, project p
where s.s# = x.s# and x.j# = p.j# and p.j# = 'J2'
;

/*
SNAME                    STATUS
-------------------- ----------
JONES                        10
BLAKE                        30
ADAMS                        30
*/


--(c) Find the name and city of each project supplied by a London-based supplier.

select distinct j.jname, j.city
from project j, supply x, supplier s
where j.j# = x.j# and x.s# = s.s# and s.city = 'LONDON'
;

/*
JNAME      CITY
---------- ---------------
SORTER     PARIS
TAPE       LONDON
CONSOLE    ATHENS
OCR        ATHENS
*/


--(d) Find the name and city of each project not supplied by a London-based supplier.

select j.jname, j.city
from project j
where not exists ( select * 
					from supplier s, supply x
					where s.city = 'LONDON' and s.s# = x.s# and j.j# = x.j#);
/*
JNAME      CITY
---------- ---------------
DISPLAY    ROME
RAID       LONDON
EDS        OSLO										
*/									 


--(e) Find the name of each supplier and the name of each part where the supplier supplies the
--part to project J7.

select s.sname, p.pname
from supply x, part p, supplier s
where x.j# = 'J7' and x.p# = p.p# and x.s# = s.s#
;
/*
SNAME                PNAME
-------------------- --------------------
JONES                SCREW
CLARK                COG
ADAMS                CAM
*/


--(f) Find the name of each supplier and the name of each part where the supplier supplies the
--part to projects J7 and J4.

select s.sname, p.pname
from supplier s, part p
where exists (select * from supply x1 where s.s#=x1.s# and p.p# = x1.p# and x1.j# = 'J7')
     and exists  (select * from supply x1 where s.s#=x1.s# and p.p# = x1.p# and x1.j# = 'J4');

/*
SNAME                PNAME
-------------------- --------------------
JONES                SCREW
ADAMS                CAM
*/


--(g) Find the supplier name, part name and project name for each case where a supplier
--supplies a project with a part and the supplier city, project city and part city are the same.

select s.sname, p.pname, j.jname
from supplier s, part p, project j, supply x
where s.s#=x.s# and p.p#=x.p# and j.j#=x.j# and s.city=p.city and s.city=j.city
;
/*
SNAME                PNAME                JNAME
-------------------- -------------------- ----------
CLARK                COG                  TAPE
*/


--(h) Find the part number of each part which is only supplied to projects based in Athens or
--Rome.

select distinct x1.p#
from supply x1
where not exists (select * from supply x2
					where x1.p# = x2.p#
					and exists (select * 
									from project j
									where x2.j#=j.j#
									and j.city <> 'ATHENS'
									and j.city <> 'ROME'
								)
				)
				;

/*
P#
------
P4
P2
*/


--(i) Find the part number of each part which is supplied to all the projects based in Athens or
--Rome.

select distinct x.p#
from supply x
where not exists (select * 
					from project j
					where (j.city = 'ROME' or j.city = 'ATHENS')
					and not exists (select *
									from supply x2
									where x.p#=x2.p# and x2.j#=j.j#
									)
				)
				;

/*
P#
------
P3
P6
*/


--(j) Find the part number of each part where there is a supplier supplying the part to all the
--projects based in Athens or Rome.

select distinct x.p#
from supply x
where not exists (select * 
					from project j
					where (j.city = 'ROME' or j.city = 'ATHENS')
					and not exists (select *
									from supply x2
									where x.s#=x2.s# and x2.j#=j.j#
									)
				)
				;
/*
P#
------
P3
P5
*/


































