--Create your own copies of the SUPPLIER, PROJECT, PART and CITIES tables as
--follows:
--SQL> CREATE TABLE SUPPLIER AS SELECT * FROM SUPPLIER;
--SQL> CREATE TABLE PART AS SELECT * FROM PART;
--SQL> CREATE TABLE PROJECT AS SELECT * FROM PROJECT;
--SQL> CREATE TABLE CITIES AS SELECT * FROM CITIES;
-- 2. Write SQL statements to answer the following queries:

--(i) For each supplier, find the supplier number, name, status and city of that supplier as
--well as the status of the city. Order result rows by supplier number.

select s.s#, s.sname, s.status, s.city , c.status as CITY_STATUS
from supplier s left join cities c
on s.city = c.city
order by s.s# asc
;

S#    SNAME                    STATUS CITY            CITY_STATUS
----- -------------------- ---------- --------------- -----------
S1    SMITH                        20 LONDON
S2    JONES                        10 PARIS                    30
S3    BLAKE                        30 PARIS                    30
S4    CLARK                        20 LONDON
S5    ADAMS                        30 ATHENS                   30

--(ii) For each city, find the status of the city and the name of each supplier in that city
--who has the same status as the city status. Order result rows by city.

select c.city, c.status , s.sname
from cities c left join supplier s
on s.city = c.city and c.status = s.status
order by c.city asc
; 

CITY                STATUS SNAME
--------------- ---------- --------------------
ATHENS                  30 ADAMS
LONDON
OSLO
PARIS                   30 BLAKE
ROME                    40
TALLINN
VIENNA


--(iii) For each city, find the total weight of the parts from that city. If a city has no parts
--from there, return a NULL value. Order result rows by city.

select c.city, sum(p.weight) as TOTAL_WEIGHT
from cities c left join part p
on c.city = p.city
group by c.city
order by c.city asc
;

CITY            TOTAL_WEIGHT
--------------- ------------
ATHENS
LONDON                    45
OSLO
PARIS                     29
ROME                      17
TALLINN
VIENNA


--(iv) For each city value in SUPPLIER or PART, find the status value of that city. Order
--result rows by city.

select a.city , c.status
from (	select * from 
			(select city from supplier 
				union 
			select city from part)) a left join cities c
on a.city = c.city
order by a.city asc
;

CITY                STATUS
--------------- ----------
ATHENS                  30
LONDON
PARIS                   30
ROME                    40


--(v) For each status value in SUPPLIER or CITIES, find the number of suppliers and the
--number of cities with that status value. Order result rows by descending status.

select x.status, x.SUPPLIER_COUNT, count(c.status) as CITY_COUNT
	from (select a.status, count(s.status) as SUPPLIER_COUNT
			from 	(select * from 
				(select status from supplier
					union
				select status from cities)
				where status IS NOT NULL) a left join
						supplier s
			on a.status = s.status
			group by a.status) x 
	left join cities c
on x.status = c.status
group by x.status , x.SUPPLIER_COUNT
order by x.status desc
;

    STATUS SUPPLIER_COUNT CITY_COUNT
---------- -------------- ----------
        40              0          1
        30              2          2
        20              2          0
        10              1          0















